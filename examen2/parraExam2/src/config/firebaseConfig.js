import { initializeApp } from '@firebase/app';

const firebaseConfig = {
  apiKey: "AIzaSyD8G40A1Ecs2hEv5MonusU9lxvo7cmibbY",
  authDomain: "fir-uni-cb6db.firebaseapp.com",
  projectId: "fir-uni-cb6db",
  storageBucket: "fir-uni-cb6db",
  messagingSenderId: "1031675583335",
  appId: "1:1031675583335:web:1edd4876e687365ec26dc2"
};

const app = initializeApp(firebaseConfig);

export default app;
